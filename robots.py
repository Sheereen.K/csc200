from gasp import * # As usual

def place_player():
    global player_x, player_y, player_shape

    player_x = random_between(0, 63) 
    player_y = random_between(0, 47)
    player_shape = Circle((10*player_x+5, 10*player_y+5), 5, filled=True)

def move_player():
    global player_x, player_y, player_shape
    key = update_when('key_pressed')

    if key == '6' and player_x < 63:
        player_x += 1
    elif key == '3':
        if player_x < 63:
            player_x += 1
        if player_y > 0:
            player_y -= 1
   
    elif key == '8' and player_y > 0:
        player_y += 1
    elif key == '1':
        if player_x < 63:
            player_x -= 1
        if player_y > 0:
            player_y -= 1
   
    elif key == '4' and player_x > 0:
        player_x -= 1
    elif key == '2':
        if player_x < 63:
            player_x -= 1
        if player_y > 0:
            player_y -= 1
  
    elif key == '9' and player_x < 0:
        player_y += 1
    elif key == '7':
        if player_x < 63:
            player_x -= 1
        if player_y > 0:
            player_y += 1

    move_to(player_shape, (10*player_x+5, 10*player_y+5))

begin_graphics()            
finished = False

place_player()

while not finished:
    move_player()

end_graphics()              
