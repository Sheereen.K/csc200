from gasp import *

begin_graphics()

#c = Circle((320, 200), 5)
#move_to(c, (5, 5))
#move_to(c, (300, 220))

def place_player():
    print("Here I am!")

def move_player():
    print("I'm moving...")
    update_when('key_pressed')
    player_x = random_between(0, 53)
    player_y = random_between(0, 47) 
    Circle((10*player_x+5, 10*player_y+5), 5, filled=True)
    c = Circle((320, 200), 5)
    move_to(c, (5, 5))
    move_to(c, (300, 220))
    ball_x = 4
    ball_y = 3
    while ball_x < 635:
        ball_x += 4 
        ball_y += 3

finished = False

while not finished:
    move_player()

place_player()
move_player()
ball_x()
end_graphics()
