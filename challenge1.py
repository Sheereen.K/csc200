from gasp import * 

def place_player():
    print("Here I am!")
    player_x = random_between(0, 53)
    player_y = random_between(0, 47)
    circle((10*player_x+10, 10*player_y+10), 10, filled=True)

def move_player():
    print("I'm moving...")
    update_when('key_pressed')

begin_graphics()
finished = False


while not finished:
    move_player()

place_player()
move_player()
